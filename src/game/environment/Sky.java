package game.environment;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import view.windows.GameWindow;

/**
 * This class create and manage all elements in the "Sky" of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class Sky extends Ambientation {

  /**
   * This field save a list all type of clouds of the game.
   */
  private List<ImageCloud> listOfCloud;
  /**
   * This field save the image of cloud.
   */
  private BufferedImage cloud;
  
  /**
   * Prepare all cloud to use in the game and set their position in the sky.
   * @param width of the game
   */
  public Sky(final int width) {
    try {
      cloud = ImageIO.read(this.getClass().getResourceAsStream("/game/environment/cloud.png"));
    } catch (IOException e) {
      System.out.println("Cloud .png Error: " + e.getMessage());
    }
    listOfCloud = new ArrayList<ImageCloud>();
    ImageCloud imageCloud = new ImageCloud();
    imageCloud.positionX = 0;
    imageCloud.positionY = 30;
    listOfCloud.add(imageCloud);
    imageCloud = new ImageCloud();
    imageCloud.positionX = 150;
    imageCloud.positionY = 40;
    listOfCloud.add(imageCloud);
    imageCloud = new ImageCloud();
    imageCloud.positionX = 300;
    imageCloud.positionY = 500;
    listOfCloud.add(imageCloud);
    imageCloud = new ImageCloud();
    imageCloud.positionX = 450;
    imageCloud.positionY = 20;
    listOfCloud.add(imageCloud);
    imageCloud = new ImageCloud();
    imageCloud.positionX = 600;
    imageCloud.positionY = 60;
    listOfCloud.add(imageCloud);
  }

  /**
   * This class describe how is made the object "Cloud" in the game.
   * 
   * @author POGGI GIOVANNI
   */
  private class ImageCloud {
    /**
     * This field set the x position of the cloud.
     */
    private float positionX;
    /**
     * This field set the y position of the cloud.
     */
    private int positionY;
  }

  @Override
  public void update(final double speed) {
    Iterator<ImageCloud> itr = listOfCloud.iterator();
    ImageCloud firstElement = itr.next();
    firstElement.positionX -= speed / 8;
    while (itr.hasNext()) {
      ImageCloud element = itr.next();
      element.positionX -= speed / 8;
    }
    if (firstElement.positionX < -cloud.getWidth()) {
      listOfCloud.remove(firstElement);
      firstElement.positionX = GameWindow.SCREEN_WIDTH;
      listOfCloud.add(firstElement);
    }
  }

  @Override
  public void drawEnvironment(final Graphics env) {
    for (ImageCloud imgLand : listOfCloud) {
      env.drawImage(cloud, (int) imgLand.positionX, imgLand.positionY, null);
    }
  }
}