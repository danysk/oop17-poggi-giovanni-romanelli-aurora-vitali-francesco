package game.mainCharacter;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * The MainCharacter class is the base abstract class that describes
 * the more important functionality of the main character.
 * 
 * @author FRANCESCO VITALI
 */
public abstract class MainCharacter {

  /**
   * This class controls the state of the main character and draws the frame
   * in a particular position with a particular appearance.
   * @param g object from Graphics class that encapsulates state information needed for the basic
   * rendering operations and here used for drawing the main character.
   */
  public abstract void draw(Graphics g);

  /**
   * This method update the frame of the main character using the class util.Animation based on
   * the state of the main character.
   */
  public abstract void update();

  /** This method manages the size of the collision bound based on if the main character is
   * running normally or jumping, or not.
   * @return the bound of the main character in a variable from the class java.awt.Rectangle
   */
  public abstract Rectangle getBound();
    
  /**
    * This method controls the main character's jumping state and also the sound of the 
    * operation.
    */
  public abstract void jump();

  /**
   * This method switches the state of main character from normal run to the run with the head
   * down.
   * @param isDown if true the method switches the state
   */
  public abstract void down(boolean isDown);

  /**
   * This method switches the state of main character from normal run to his death state.
   * It also manages the sound operation.
   * @param isDeath if true the method switches the state
   */
  public abstract void dead(boolean isDeath);

  /**
   * This method resets the main character in his starting position.
   */
  public abstract void reset();

}