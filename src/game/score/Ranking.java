package game.score;

import java.util.ArrayList;

/**
 * Interface to manipulate a ranking.
 * @author ROMANELLI AURORA
 * 
 */
public interface Ranking {
    
  /**Getter.
 * @return the list containing the ranking like HighScores object.
 */
  public ArrayList<HighScore> getScores();
  
  /**Try to add a player and his score in the ranking.
 * @param name of the player.
 * @param score obtained in the game.
 */
  public void addScore(String name, int score);
  
  /**Getter.
 * @return the ranking like a string.
 */
  public String getHighscoreString();
  
  /**
   * Getter of the name of the player in a fixed position in the ranking.
 * @param i position.
 * @return the name of the player.
 */
  public String getNameAtPosition(int i);
  
  /**
   * Getter of the score of a fixed position in the ranking.
 * @param i position.
 * @return the score.
 */
  public int getScoreAtPosition(int i);
  
  /**
 * Delete the current ranking file.
 */
  public void clear();
  
  /**Check.
 * @return if the file is Present.
 */
  public boolean isPresent();

}
