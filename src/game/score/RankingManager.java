package game.score;

import game.score.HighScore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This class manipulate the file for the ranking.
 * 
 * @author ROMANELLI AURORA
 *
 */
public class RankingManager implements Ranking {
    
  private static final int NUM_PLAYER_RANKING = 10;
  private static final String HIGHSCORE_FILE = "scores.dat";
  private static final String SP = File.separator;
  
  private ArrayList<HighScore> scores;
  
  /**
   * Constructor.
   */
  public RankingManager() {
    scores = new ArrayList<HighScore>();
    loadScoreFile();
  }
  
  @Override
  public ArrayList<HighScore> getScores() {
    loadScoreFile();
        
    return scores;
  }
    
  @Override
  public void addScore(final String name, final int score) {
    scores.add(new HighScore(name, score));
    sort();
    if (scores.size() == NUM_PLAYER_RANKING + 1) {
      scores.remove(NUM_PLAYER_RANKING);
    }
    updateScoreFile();
  }

    
  @Override
  public String getHighscoreString() {
    String highscoreString = "";
    final int max = 10;
    ArrayList<HighScore> scores;
    scores = getScores();
    int i = 0;
    int x = scores.size();
    
    if (x > max) {
      x = max;
    }
    
    while (i < x) {
      highscoreString += (i + 1) + ".\t" + scores.get(i).getScore() 
              + "\t\t" + scores.get(i).getName() + "\n";
      i++;
    }
    return highscoreString;
  }
    
  @Override
  public String getNameAtPosition(final int i) {
    return scores.get(i).getName();
  }
    
  @Override
  public int getScoreAtPosition(final int i) {
    if (!scores.isEmpty()) {
      return scores.get(i).getScore();
    } else {
      return 0;
    }
  }
  
  @Override
  public void clear() { 
    String jarPath = "";
    String completePath = "";
    try {
      jarPath = URLDecoder.decode(getClass().getProtectionDomain().getCodeSource()
              .getLocation().getPath(), "UTF-8");
    } catch (final UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    completePath = jarPath.substring(0, jarPath.lastIndexOf("/")) 
                + SP + HIGHSCORE_FILE;
    final File f = new File(completePath);
    if (f.exists()) {
      try {
        f.delete();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }
    
  @Override
  public boolean isPresent() {
    String jarPath = "";
    String completePath = "";
    try {
      jarPath = URLDecoder.decode(getClass().getProtectionDomain().getCodeSource()
              .getLocation().getPath(), "UTF-8");
    } catch (final UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    completePath = jarPath.substring(0, jarPath.lastIndexOf("/")) 
              + SP + HIGHSCORE_FILE;
    final File f = new File(completePath);
    return f.exists();
  }
 
  private void updateScoreFile() {
                   
    String jarPath = "";
    try {
      jarPath = URLDecoder.decode(getClass().getProtectionDomain().getCodeSource()
              .getLocation().getPath(), "UTF-8");
    } catch (final UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }

    final String completePath = jarPath.substring(0, jarPath.lastIndexOf("/")) 
                            + SP + HIGHSCORE_FILE;
    final File f = new File(completePath);
    try {
      if (f.exists() || f.createNewFile()) {
        final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
        out.writeObject(scores);
        out.close();
      }
                         
    } catch (final Exception e) {
      e.printStackTrace();
    }
                     
  }
                 
  @SuppressWarnings("unchecked")
private void loadScoreFile() {
    String jarPath = "";
    String completePath = "";
    try {
      jarPath = URLDecoder.decode(getClass().getProtectionDomain().getCodeSource()
              .getLocation().getPath(), "UTF-8");
     
    } catch (final UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    completePath = jarPath.substring(0, jarPath.lastIndexOf("/")) 
             + SP + HIGHSCORE_FILE;
    final File f = new File(completePath);
    if (f.exists()) {
      try {
        final ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
                                
        scores = (ArrayList<HighScore>) in.readObject();
        in.close();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    } else {
      this.updateScoreFile();
    }
  }
                
  private void sort() {
    final ScoreComparator comparator = new ScoreComparator();
    Collections.sort(scores, comparator);
  }               

}
