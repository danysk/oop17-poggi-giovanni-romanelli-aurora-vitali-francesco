package util;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * This class manages the animation of the main character
 * and chooses when to use each frame.
 * 
 * @author FRANCESCO VITALI, POGGI GIOVANNI
 */
public class Animation {

  /**
   * This field saves in a list all frames of the main character.
   */
  private List<BufferedImage> list;
  /**
   * This field saves the time to wait before changing the frame
   * of the main character.
   */
  private long deltaTime;
  /**
   * This field saves the current frame that the game uses.
   */
  private int currentFrame = 0;
  /**
   * This field saves the previous time of the running game.
   */
  private long previousTime;

  /**
   * Prepares the field and creates the animation
   * of the main character.
   * @param deltaTime is the time to wait to change frame of the main character.
   */
  public Animation(final int deltaTime) {
    this.deltaTime = deltaTime;
    list = new ArrayList<BufferedImage>();
    previousTime = 0;
  }

  /**
   * This method changes the frame of the main character
   * if the delta time is passed.
   */
  public void updateFrame() {
    if (System.currentTimeMillis() - previousTime >= deltaTime) {
      currentFrame++;
      if (currentFrame >= list.size()) {
        currentFrame = 0;
      }
      previousTime = System.currentTimeMillis();
    }
  }

  /**
   * This method adds a frame in the list of all frames.
   * @param image the frame to add
   */
  public void addFrame(final BufferedImage image) {
    list.add(image);
  }

  /**
   * This method returns the frame that the game is using.
   *  @return the current frame
   */
  public BufferedImage getFrame() {
    return list.get(currentFrame);
  }
  
}