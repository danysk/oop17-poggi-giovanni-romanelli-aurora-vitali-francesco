package controller.scene;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import util.Difficulty;
import view.sceneLoader.FXMLPath;
import view.windows.GameWindow;

/**
 * This Class manage the choose of the user of the game's mode.
 * 
 * @author POGGI GIOVANNI
 */
public class ModeController {

  /**
   * Button back of the scene.
   */
  @FXML
  private Button back;
  /**
   * Button classic mode of the scene.
   */
  @FXML
  private Button classic;
  /**
   * Button easy mode of the scene.
   */
  @FXML
  private Button easy;
  /**
   * Button medium mode of the scene.
   */
  @FXML
  private Button medium;
  /**
   * Button hard mode of the scene.
   */
  @FXML
  private Button hard;
  @FXML
  private Pane rootPane;

  /**
   * This method return to the main scene of the game.
   * 
   * @param event of the button.
   * @throws IOException is the method fails to load scene.
   */
  @FXML
  private void pressBack(ActionEvent event) throws IOException {
    this.rootPane.getChildren().clear();
    Parent root = FXMLLoader.load(getClass().getResource(FXMLPath.GAME.getPath()));
    this.rootPane.getChildren().add(root);
  }

  /**
   * This method start the game in classic mode.
   * 
   * @param event of the button.
   */
  @FXML
  private void pressClassic(ActionEvent event) {
    Button btn = (Button)event.getSource();
    btn.getScene().getWindow().hide();
    new GameWindow().startGame();
  }

  /**
   * This method start the game in easy mode.
   * 
   * @param event of the button.
   * @throws IOException is the method fails to load scene.
   */
  @FXML
  private void pressEasy(ActionEvent event) throws IOException {
    new GameWindow().startFixedGame(Difficulty.EASY);
    Button btn = (Button)event.getSource();
    btn.getScene().getWindow().hide();
  }

  /**
   * This method start the game in medium mode.
   * 
   * @param event of the button.
   * @throws IOException is the method fails to load scene.
   */
  @FXML
  private void pressMedium(ActionEvent event) throws IOException {
    new GameWindow().startFixedGame(Difficulty.MEDIUM);
    Button btn = (Button)event.getSource();
    btn.getScene().getWindow().hide();
  }

  /**
   * This method start the game in hard mode.
   * 
   * @param event of the button.
   * @throws IOException is the method fails to load scene.
   */
  @FXML
  private void pressHard(ActionEvent event) throws IOException {
    new GameWindow().startFixedGame(Difficulty.HARD);
    Button btn = (Button)event.getSource();
    btn.getScene().getWindow().hide();
  }
}