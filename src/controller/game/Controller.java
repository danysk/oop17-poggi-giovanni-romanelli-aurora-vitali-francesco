package controller.game;


/**
 * Interface of Controller.
 * 
 * @author ROMANELLI AURORA
 *
 */
public interface Controller {
    
  /**
   * Method that launch the game's thread.
   */
  public void gameStart();

  /**
   * Method that kill the game's thread.
   */
  public void gameOver();

  /**
   * Method that include all update (enemies, main character, score ...).
   */
  public void gameUpdate();

  /**
   * Method that include all reset (enemies, main character, score ...).
   */
  public void gameReset();

  /**
   * Method that add a new player in the game ranking.
   * @param name username of player.
   */
  public void addToRanking(final String name);

}
